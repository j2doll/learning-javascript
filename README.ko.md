# 러닝 자바스크립트
ES6로 제대로 입문하는 모던 자바스크립트 웹 개발

> *Read this in other languages: [English](README.md), [한국어](README.ko.md)*

![](image/lrg.jpg)

이선 브라운 저. 한선용 역.

## 출판사 서평 (한빛)
ES6에 맞춰 고품질 웹 앱을 더 쉽고 빠르게 개발하는 방법!

자바스크립트 전문가이자 전도사인 이선 브라운이 『한 권으로 끝내는 Node & Express』(2015, 한빛미디어)에 이어 또 한권의 멋진 자바스크립트 입문서를 펴냈다. 차세대 표준인 ES6에 맞춘 이 책은 기초적인 트랜스컴파일러 사용법부터 시작해 날짜와 시간, 수학 라이브러리, 정규식 등 실용적인 필수 주제를 적절히 배치해 설명하며, ES6의 변경 사항과 핵심 요소도 알기 쉽게 전달한다. 이미 여러 권의 IT 전문서를 번역한 한선용 역자의 깔끔한 번역도 빛을 발하는데, 특히 버전마다 문제가 될 만한 부분에는 별도의 코멘트로 해결책을 달아 꼼꼼하게 내용을 보완했다. 자바스크립트 경험이 있는 독자, 특히 ES5만 사용해 본 독자라면 주요한 언어 개념에 대한 현실적이고 자세한 설명에 만족할 것이다. 아직 자바스크립트를 잘 모르는 독자라 해도, 다른 언어로 개발해 본 경험이 있고 새롭게 자바스크립트를 배워보고자 한다면 큰 도움이 될 것이다.

### 주요 내용
- ES6 코드 작성 및 포터블 ES5로 트랜스컴파일
- 자바스크립트에서 사용하는 포맷으로 데이터 변환
- 자바스크립트 함수의 기본 사용법과 메커니즘 이해
- 객체와 객체 지향 프로그래밍
- 이터레이터, 제너레이터, 프락시 등 새로운 개념 이해
- 복잡한 비동기 프로그래밍 이해
- 브라우저 기반 애플리케이션을 위한 DOM 이해
- 서버 측 애플리케이션 개발을 위한 Node.js 기본 습득

### 대상 독자
- ES6로 자바스크립트에 입문하려는, ‘타 언어 경험이 있는’ 개발자
- 자바스크립트 기본 지식이 있고, ES5 경험이 있는 웹 개발자
- 다른 프로그래밍 언어 경험이 있고, 최신 자바스크립트를 빠르게 익혀 실무에 활용하려는 웹  개발자(자바스크립트나 노드를 처음 사용하는 ‘숙련된’ 개발자)
