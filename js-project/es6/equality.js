// equality.js

{
  const n = 5;
  const s = "5";

  console.log( n === s ); // false. type is different.
  console.log( n !== s ); // true

  console.log( n === Number(s) ); // true
  console.log( n !== Number(s) ); // false

  console.log( n == s ); // true. not recommended.
  console.log( n != s ); // false. not recommended.
}

{
  const a = { name : "an object" };
  const b = { name : "an object" };

  console.log( a === b );  // false. object is 'always' not equal.
  console.log( a !== b );  // true

  console.log( a == b );  // false. not recommended.
  console.log( a != b );  // true. not recommended.
}
