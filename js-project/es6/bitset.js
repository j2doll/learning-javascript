// bitset.js

const FLAG_EXECUTE = 1 // 0b001
const FLAG_WRITE   = 2 // 0b010
const FLAG_READ    = 4 // 0b100

let p = FLAG_READ | FLAG_WRITE;  // 0b110
console.log( p & FLAG_WRITE );   // 0b010
console.log( p & FLAG_EXECUTE ); // 0b000
p = p ^ FLAG_WRITE; // 0b100  toggle write flag
p = p ^ FLAG_WRITE; // 0b110  toggle write flag

const hasReadOrExecute = p & (FLAG_READ | FLAG_EXECUTE); // 0b100
const hasReandAndExecute = p & (FLAG_READ | FLAG_EXECUTE) === FLAG_READ | FLAG_EXECUTE; // 0b001

console.log( ' hasReadOrExecute : ' + hasReadOrExecute );
console.log( ' hasReandAndExecute : ' + hasReandAndExecute );
