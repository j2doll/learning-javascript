// this.js

const o = {
    name: "Wallace",
    speak() { return `My name is ${this.name}!`; },
}
console.log( o.speak() ); // My name is Wallace!

const speak = o.speak;
console.log( speak === o.speak ); // true
console.log( speak() ); // My name is undefined!


