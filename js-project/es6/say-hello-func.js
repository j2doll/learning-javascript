// say-hello-func.js

// function declaration 
function sayHello() {
    console.log("Hello function!"); 
}

// call fucntion 
sayHello();

function getGreeting() {
    return "Hello world!";
}

console.log( getGreeting() ); // Hello world!

// console.log( getGreeting ); // function getGreeting()
//              getGreeting() is not executed! just referenced

const f = getGreeting;
console.log( f() ); // Hello world!     

const o = {};
o.f = getGreeting; // set function to a property of object 
console.log( o.f() ); // Hello world!     

const arr = [1, 2, 3];
arr[1] = getGreeting; 
console.log( arr[1]() ); // Hello world!
console.log( arr ); // Array(3) [1, function getGreeting(), 3]

