// deassign-param.js

{
    function getSentence({ subject, verb, object }) {
        return `${subject} ${verb} ${object}`;
    }
    
    const o = {
        subject: "I",
        verb: "love",
        object: "JavaScript"
    };
    console.log( getSentence(o) ); // I love JavaScript
}

{
    function getSentenceArray([ subject, verb, object ]) {
        return `${subject} ${verb} ${object}`;
    }
    
    const arr = ["I", "love", "JavaScript"];
    console.log( getSentenceArray(arr) ); // I love JavaScript
}

{
    function addPrefix(prefix, ... words) {
        const prefixedWords = [];
        for ( let i = 0; i < words.length; i++ ) {
            prefixedWords[i] = prefix + words[i];
        }
        return prefixedWords;
    }
    
    console.log( addPrefix("con", "verse", "vex") ); // Array(2) ["converse", "convex"]
}

