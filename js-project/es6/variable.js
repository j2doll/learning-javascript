// es6/variable.js
//  this file is ignored by gulp.

let currentTempC = 22; // celcius 
currentTempC = 23;

let targetTempC, room1 = 'conference room a', room2 = 'lobby';

const ROOM_TEMP_C = 21.5, MAX_TEMP_C = 30;

let count = 10; // literal. count is double type.

const blue = 0x0000ff; // hex
const umask = 0o0022; // octal
const rootTemp = 21.5; // decimal

const c = 3.0e6;
const e = -1.6e-19; 

const inf = Infinity;
const ninf = -Infinity;

const nan = NaN; // not a number 


