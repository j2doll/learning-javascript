// alloc-operator.js

let x, v0;
v = v0 = 9.8; // first set v0 = 9.8, then set v = 9.8.

const nums = [ 3, 5, 15, 7, 5 ];

let n, i = 0;
while ( (n = nums[i]) < 10 && i ++ < nums.length ) {
    console.log( `Number less than 10: ${n}.` );
}
// Number less than 10: 3.
// Number less than 10: 5.

console.log(`Number greater than 10 found: ${n}.`); // Number greater than 10 found: 15.

console.log(`${nums.length -i - 1} numbers remain.`); // 2 numbers remain.




