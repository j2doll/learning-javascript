// func-param.js

{
    function avg(a, b) {
        return (a+b) / 2;
    }

    console.log( avg(5, 10) ); // 7.5

    const a = 5, b= 10; // these a and b is const value, not function parameter a and b.
    console.log( avg(a, b) ); // 7.5
}

{
    function f1(x) {
        console.log(`f1(x) 내부: x = ${x}`);
        x = 5;
        console.log(`f1(x) 내부: x = ${x} (할당후)`);
    }

    let x = 3;
    console.log(`f1(x)를 호출하기 전: x = ${x}`);
    f1(x);
    console.log(`f1(x)를 호출한 다음: x = ${x}`);
    /*
        f1(x)를 호출하기 전: x = 3
        f1(x) 내부: x = 3
        f1(x) 내부: x = 5 (할당후)
        f1(x)를 호출한 다음: x = 3
    */
}

{
    // object parameter 

    function f2(o) {
        o.message = `f2(o) 안에서 수정함 (이전값: ${o.message})`;
    }

    let o = {
        message: "o 초기값",
    }

    console.log( `f2(o)를 호출하기 전: o.message = ${o.message}` );
    f2(o);
    console.log( `f2(o)를 호출한 다음: o.message = ${o.message}` ); 

    /*
    f2(o)를 호출하기 전: o.message = o 초기값

    f2(o)를 호출한 다음: o.message = f2(o) 안에서 수정함 (이전값: o 초기값)
    */
}

{
    function f3(o) {
        o.message = "f3(o)에서 수정함";
        o = { message: "새로운 객체" };
        console.log(`f3(o) 내부: o.message = ${o.message} (할당 후)`);
    }

    let o3 = { message: "o3 초기값" };
    console.log(`f3(o)를 호출하기 전: o3.message = ${o3.message} `);
    f3(o3);
    console.log(`f3(o)를 호출후: o3.message = ${o3.message} `);

    /*
	f3(o)를 호출하기 전: o3.message = o3 초기값 
	f3(o) 내부: o.message = 새로운 객체 (할당 후)
	f3(o)를 호출후: o3.message = f3(o)에서 수정함 
    */
}

{
    function f4(x) {
        return `in f: x = ${x}`;
    }
    console.log( f4() ); // in f: x = undefined
    console.log( f4( 100, 200 ) ); // in f: x = 100
}



