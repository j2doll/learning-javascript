// array2.js

function show_props(obj) { // obj is array.
    let printValue = "";
    for ( let index = 0 ; index < obj.length ; index++ ) {
        let nameValue = obj[index].name;
        // let addressValue = obj[index].address;
        // if ( addressValue == null ) { } // empty value
        printValue = printValue + '(index = ' + index + ', name : ' + nameValue + ') ';
    }
    console.log( printValue );
}

const arr = [ {name : 'Suzanne', address : 'Paris'}, {name : 'Jim'} ];

show_props(arr); // (index = 0, name : Suzanne) (index = 1, name : Jim) 
arr.sort( (a,b) => a.name > b.name );
show_props(arr); // (index = 0, name : Jim) (index = 1, name : Suzanne) 

show_props(arr); // (index = 0, name : Jim) (index = 1, name : Suzanne) 
arr.sort( (a,b) => a.name[1] < b.name[1] );
show_props(arr); // (index = 0, name : Suzanne) (index = 1, name : Jim) 


