// call.js

const bruce = { name : "Bruce" };
const madeline = { name : "Madeline" };

function greet() {
  return `Hello, I'm ${this.name}.`
}

// call

console.log( greet() ); // undefined
console.log( greet.call(bruce) );
console.log( greet.call(madeline) );

function update(birthYear, occupation) {
  this.birthYear = birthYear;
  this.occupation = occupation;
}

update.call(bruce, 1949, 'singer');
console.log(bruce);
update.call(madeline, 1942, 'actress');
console.log(madeline);

// apply

const arr = [2, 3, -5, 15, 7];
console.log( Math.min.apply(null, arr) ); // -5
console.log( Math.max.apply(null, arr) ); // 15

// ...

const newBruce = [1940, "material artist"];
update.call(bruce, ...newBruce);
console.log( Math.min(...arr) ); // -5
console.log( Math.max(...arr) ); // 15

// bind

const updateBruce = update.bind(bruce); // change this to 'bruce'
updateBruce(1904, 'actor');
console.log(bruce); // { name: 'Madeline', birthYear: 1942, occupation: 'actress' }

updateBruce.call(madeline, 1274, 'king');
console.log(bruce); // { name: 'Bruce', birthYear: 1274, occupation: 'king' }
console.log(madeline); // { name: 'Madeline', birthYear: 1942, occupation: 'actress' }

const updateBruce1949 = update.bind(bruce, 1949); // change this to 'bruce' and change birthYear to '1949' 
updateBruce1949("singer, song-writer");
console.log(bruce); // { name: 'Bruce', birthYear: 1949,  occupation: 'singer, song-writer' }
