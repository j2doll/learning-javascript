// destructing-assignment.js

{
    const obj = { b: 2, c: 3, d: 4 };

    const {a, b, c} = obj;

    console.log( a ); // undefined
    console.log( b ); // 2
    console.log( c ); // 3
    console.log( d ); // Error: ReferenceError: d is not defined
}

{
    const obj = { b: 2, c: 3, d: 4 };
    let a, b, c;

    {a, b, c} = obj; // Error! 

    ({a, b, c} = obj); // it works!
}

{
    const arr = [1, 2, 3];

    let [x, y] = arr;

    console.log( x ); // 1  
    console.log( y ); // 2
    console.log( z ); // ReferenceError: z is not defined
}

{
    const arr = [ 1, 2, 3, 4, 5 ];

    let [ x, y, ... rest ] = arr; 

    console.log( x ); // 1
    console.log( y ); // 2 
    console.log( rest ); // [3, 4, 5] 
}

{
    let a= 5, b= 10;

    [a, b] = [b, a]; // [a, b] = [10, 5];

    console.log( a ); // 10   
    console.log( b ); // 5   
}
