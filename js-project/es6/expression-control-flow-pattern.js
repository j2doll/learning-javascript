// expression-control-flow-pattern.js

{
    function isPrime(n)
    {
        return false;
    }

    let label;
    let n = 10;
    
    if (isPrime(n)) {
        label = 'prime';
    } else {
        label = 'non-prime';
    }
    
    // easy expression 
    label = isPrime(n) ? 'prime' : 'non-prime';
}

{
    let options; 

    if (!options) options = {};
    
    options = options || {}; // same code. Frequently used codes.
}


