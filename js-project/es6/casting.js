// casting.js

/////////////////////////////
// to number

const numStr = '33.3';
const nums = Number(numStr); 
console.log( nums );

const numNan = 'what';
const numn = Number(numNan); 
console.log( numn ); // NaN


const a = parseInt('16 volts', 10);
console.log( a ); // 16 

const b = parseInt('3a', 16);
console.log( b ); // 58 

const c = parseFloat('15.5 kph');
console.log( c ); // 15.5
 
 
const d = new Date();
const ts = d.valueOf();
console.log( ts ); // epoch time (unit is mil.second)

const bl = true;
const nbl = bl ? 1 : 0;
console.log( nbl ); // 1 


/////////////////////////////
// to string 

const nValue = 35.5; // nValue is a number 

const sValue = nValue.toString(); // sValue is a string



const arr = [1, true, 'hello'];
const arrStr = arr.toString();
console.log( arrStr ); // 1,true,hello


/////////////////////////////
// to boolean


const n = 0;
const b0 = !n;
const b1 = !!n;
const b2 = Boolean(n); 

console.log( b0 ); // true 
console.log( b1 ); // false 
console.log( b2 ); // false 



