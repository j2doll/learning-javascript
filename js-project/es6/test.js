// test.js

'use strict';

const sentences = [
	{ subject: 'JavaScript', verb: 'is', object: 'great' },
	{ subject: 'Elephants', verb: 'are', object: 'large' },
];

function say ({ subject, verb, object }) {
	console.log( `${subject} ${verb} ${object}` + '.' );
}

for (let s of sentences) {
	say(s);
}

// .eslintrc.js is follows.
/* 
	module.exports = {
		"env": { "es6": true, "node": true },
		"extends": "eslint:recommended",
		"parserOptions": { "sourceType": "module" },
		"rules": {
			"no-console":0,
			"indent": [ "error", "tab" ],
			"linebreak-style": [ "error", "unix" ],
			"quotes": [ "error", "single"],
			"semi": [ "error", "always" ]
		}
	};
*/
