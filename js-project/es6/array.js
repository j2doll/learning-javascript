// es6/array.js

const a1 = [1, 2, 3, 4]; // array that is consist of number

const a2 = [1, 'two', 3, null]; // array that is consist of variant value

const a3 = [ // multi-line array 
	'What the hammer? What the chain?',
	'In what furnace was thy brain?',
	'What the anvil? What dread grasp',
	'Dare its deadly terrors clasp?',
];

const a4 = [ // object array 
	{ name : 'Ruby', hardness : 9 },
	{ name : 'Diamond', hardness : 10 },
	{ name : 'Topaz', hardness : 8 },	
];

const a5 = [ // array that is consist of array 
	[1, 3, 5],
	[2, 4, 6],
]; 

const arr = ['a', 'b', 'c'];
console.log( arr.length ); // 3 
console.log( arr[0] ); // 'a'
console.log( arr[arr.length - 1] ); // 'c'

const arr2 = [1, 2, 'c', 4, 5];
arr2[2] = 3;
console.log( arr2 ); // [1, 2, 3, 4, 5]








