// function-expression-and-anonymous-function.js

function currentTime() {
  var today = new Date();
  var h = today.getHours();
  var m = today.getMinutes();
  var s = today.getSeconds();
  var ms =  today.getMilliseconds();

  return `${h} : ${m} : ${s}.${ms}`;
}

// g() is anonymous function 
const g = function f(stop) {  
    if (stop) {
        console.log( currentTime() + ' f.stopped' );
    }
    f(true); // recursion
}

g(false);



