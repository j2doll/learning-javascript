// boolean-js.js

/*
  false of javascript
    - undefined
    - null
    - false
    - 0
    - NaN
    - '' (empty string)

  true of javascript : everything except false.
*/

console.log( undefined );
console.log( null );
console.log( false );
console.log( 0 );
console.log( NaN );
console.log( '' );
console.log( eval( " " ) );

