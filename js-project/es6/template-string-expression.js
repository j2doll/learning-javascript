// template-string-expression.js

const roomTempC = 21.5;

let currentTempC = 19.5; 

const message = `The current temperature is ` + 
    `${currentTempC - roomTempC}\u00b0c diffrent than room temperature.`;

const fahrenheit =
    `The current temperature is ${currentTempC * 9/5 + 32}\u00b0F`    

console.log( message ); // The current temperature is -2°c diffrent than room temperature.
console.log( fahrenheit ); // The current temperature is 67.1°F

