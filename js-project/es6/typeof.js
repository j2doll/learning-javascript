// typeof.js

/*
- seven types of javascript (ES6)
  - number
  - string
  - boolean
  - null
  - undefined
  - symbol
  - object
*/

console.log( typeof null ); // object
console.log( typeof [] ); // object
console.log( typeof undefined ); // undefined
console.log( typeof {} ); // object
console.log( typeof true ); // boolean
console.log( typeof 1 ); // number
console.log( typeof "" ); // string
console.log( typeof Symbol() ); // symbol
console.log( typeof function() {} ); // function
