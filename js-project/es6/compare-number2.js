// comapre-number2.js

function sleep(ms){
  ts1 = new Date().getTime() + ms;
  do ts2 = new Date().getTime(); while (ts2<ts1);
}

let n = 0;
while (true) {
  n += 0.1;
  console.log('n : ' + n);
  if ( Math.abs(n-0.3) < Number.EPSILON )
    break;
  sleep(1000);
}

console.log(`Stopped at ${n}`);
