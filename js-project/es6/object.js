// object.js

const obj = {};

obj.color = 'yellow';

obj['not an identifier'] = 3;

console.log( obj['not an identifier'] ); // 3 
console.log( obj['color'] ); // 'yellow'



const SIZE = Symbol();

obj[SIZE] = 8;
console.log( obj[SIZE] ); // 8



const sam1 = {
	name : 'Sam',
	agr : 4,
};

const sam2 = { name : 'Sam', agr : 4 };

const sam3 = {
	name : 'Sam',
	classification : {
		kingdom : 'Anamalia',
		phylum : 'Chordata',
		class : 'Mamalia',
		order : 'Carnivoria',
		family : 'Felidae',
		subfamily : 'Felinae',
		genus : 'Felis',
		species : 'catus',
	},
};

// Felidae
console.log( sam3.classification.family ); 
console.log( sam3['classification'].family );
console.log( sam3.classification['family'] );
console.log( sam3['classification']['family'] );

sam3.speak = function() { return 'Meow!'; };
console.log( sam3.speak() ); 

delete sam3.classification;
delete sam3.speak; 



