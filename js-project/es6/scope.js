// scope.js


function f1(x) {
    return x + 3;
}

 console.log( f1(5) ); // 8 
// console.log( x ); // ReferenceError: x is not defined

function f2() {
    console.log('two');
}

function f3() {
    console.log('three');
}

f3(); // three
f2(); // two
f3(); // three

const a = 3;

function f4() {
    console.log(a);
    // console.log(b); // b is not defined (static scope)
}

{
    const b = 5;
    f4();  
}

let name = "Irena"; // global scope
let age = 25; // global scope

function greet() {
    console.log(`Hello,${name}!`); 
}

function getBirthYear() {
    return new Date().getFullYear() - age; 
}

greet(); // Hello,Irena!
console.log( getBirthYear() ); // 1993

let user = {
    name = "Irena",
    age = 25,
};

function greet2(user) {
    console.log(`Hello,${user.name}!`); 
}

function getBirthYear2(user) {
    return new Date().getFullYear() - user.age; 
}

greet2(user);
console.log( getBirthYear2(user) ); 















