// non-boolean-value.js

 var suppliedOptions1 = new Object();
const suppliedOptions2 = true;
const suppliedOptions3 = null;

const options1 = suppliedOptions1 || { name : "Default" }
const options2 = suppliedOptions2 || { name : "Default" }
const options3 = suppliedOptions3 || { name : "Default" }

console.log( ' options1 : ' + options1 +
             ', options1 : ' + options2 +
             ', options3 : ' + options3 );
// options1 : [object Object], options1 : true, options3 : [object Object]
