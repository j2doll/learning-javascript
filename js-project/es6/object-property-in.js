// object-property-in.js

const o = {
    name: "Wallace", // premitive value property
    bark: function() { return "Woof!";} // function preperty (method)
}

const oES6 = {
    name: "Wallace", // premitive value property
    bark() { return "Woof!";} // function preperty (method)
}

console.log( o.name ); // Wallace
console.log( o.bark() ); // Woof!
console.log( oES6.name ); // Wallace
console.log( oES6.bark() ); // Woof!



