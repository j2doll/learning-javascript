// short-circuit-evaluation.js

// OR case of short circuit evaluation
{
  const skipIt = true;
  let x = 0, y = 0;

  const result = skipIt||x++;
  const result2 = y++;

  console.log( ' x = ' + x + ', y = ' + y ); // x = 0, y = 1
}

// AND case of short circuit evaluation
{
  const doIt = false;
  let x = 0;
  const result = doIt && x++;

  console.log( ' x = ' + x + ', result = ' + result ); // x = 0, result = false
}

{
  const doIt = true;
  let x = 0;
  const result = doIt && x++;

  const result2 = true && 0; // 0
  const result3 = true && 1; // 1

  console.log( ' x = ' + x + ', result = ' + result ); // x = 1, result = 0
  console.log( ' result2 = ' + result2 + ', result3 = ' + result3 ); // result2 = 0, result3 = 1
}
