// bit-operator.js

function createBinaryString(nMask) {
  // nMask must be between -2147483648 and 2147483647
  for (var nFlag = 0, nShifted = nMask, sMask = "";
        nFlag < 32;
        nFlag++, sMask += String(nShifted >>> 31), nShifted <<= 1 );
  return sMask;
}

let n = 22;

console.log(createBinaryString( n ));      // 00000000000000000000000000010110
console.log(createBinaryString( n >> 1 )); // 00000000000000000000000000001011
console.log(createBinaryString( n >>> 1 ));// 00000000000000000000000000001011
console.log(createBinaryString( n = ~n )); // 11111111111111111111111111101001
console.log(createBinaryString( ++n ));    // 11111111111111111111111111101010
console.log(createBinaryString( n >> 1 )); // 11111111111111111111111111110101
console.log(createBinaryString( n >>> 1 ));// 01111111111111111111111111110101

// n = ~n  : 1's complement
// n++     : 2's complement
