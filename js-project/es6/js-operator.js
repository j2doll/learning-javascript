// js-operator.js

{
	// number and string 

	const x = 5;
	const y = 3- -x; // y is 8

	console.log( ' y is '+ y );
}

{
	const s = "5";
	const y = 3 + +s; // y is 8	
	const z = 3 + s; // z is 35

	console.log( ' y is '+ y ); 
	console.log( ' z is '+ z ); 

	const x1 = 0, x2 = 3, x3 = -1.5, x4 = -6.33;
	const p1 = -x1*1; // 0
	const p2 = +x2*2; // 6
	const p3 = +x3*3; // -4.5
	const p4 = -x4*4; // 25.32	

	console.log( `${p1} ${p2} ${p3} ${p4}` ); 
}

{
	// mod 

	console.log( 10 % 3 ); // 1	
	console.log( 10 % 3.6 ); // 2.8	
}

{
	// prefix postfix operator

	let x = 2;
	
	const r1 = x++ + x++; 
	//         ((x++) + (x++))   now x is 2
	//         ( 2    + (x++))   now x is 3
	//         ( 2    +  3   )   now x is 4
	//           5 
	console.log( ' r1 = ' + r1 + ' x = ' + x ); // r1 = 5 x = 4

	const r2 = ++x + ++x; 
	console.log( ' r2 = ' + r2 + ' x = ' + x ); // r2 = 11 x = 6
	
	const r3 = x++ + ++x; 
	console.log( ' r3 = ' + r3 + ' x = ' + x ); // r2 = 14 x = 8

	const r4 = ++x + x++; 
	console.log( ' r4 = ' + r4 + ' x = ' + x ); // r2 = 18 x = 10	

}


{
	// Operator Precedence

	let x = 3, y;
	x += y = 6*5/2;

//	x += y = (6*5)/2
//	x += y = 30/2
//	x += y = 15
//	x += (y = 15)
//	x += 15         now y is 15
//  x = x + 15
//  x = 3 + 15
//  x = 18          now x is 18 

	console.log( ' x = ' + x + ', y = ' + y ); // x = 18, y = 15 	

}


{
	// 'strict equality'and 'loosely equality'
	//  === !==               == !=  

	const n = 5; 

}


