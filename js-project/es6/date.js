// date.js

const now = new Date();
console.log( now ); // 2018-04-09T10:37:08.477Z

const halloween = new Date(2016, 9, 31); // month from 0. 9 is december.
console.log( halloween ); // 2016-10-30T15:00:00.000Z

const halloweenParty = new Date(2016, 9, 31, 19, 0); // 19:00 = 7:00 pm
console.log( halloweenParty ); // 2016-10-31T10:00:00.000Z

console.log( halloweenParty.getFullYear() ); // 2016
console.log( halloweenParty.getMonth() ); // 9
console.log( halloweenParty.getDate() ); // 31
console.log( halloweenParty.getDay() ); // 1.  (1 is monday. 0 is sunday.)
console.log( halloweenParty.getHours() ); // 0
console.log( halloweenParty.getMinutes() ); // 0
console.log( halloweenParty.getSeconds() ); // 0
console.log( halloweenParty.getMilliseconds() ); // 0








