// gulpfile.js

// set gulp dependency 
const gulp = require('gulp');
const babel = require('gulp-babel');
const eslint = require('gulp-eslint');

gulp.task( 'default', function() {

	// execute ESLint
	gulp.src(['es6/**/*.js', 'public/es6/**/*.js', '!es6/variable.js'])
	.pipe( eslint() )
	.pipe( eslint.format() );

	// node source 
	gulp.src('es6/**/*.js')
	.pipe( babel() )
	.pipe( gulp.dest( 'dist' ) );

	// browser source 
	gulp.src( 'public/es6/**/*.js' )
	.pipe( babel() )
	.pipe( gulp.dest( 'public/dist' ) ); 

} );

